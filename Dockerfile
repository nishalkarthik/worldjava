FROM jboss/wildfly
#ADD samplewar/target/samplewar.war /opt/jboss/wildfly/standalone/deployments/
ADD http://54.188.94.254:8081/repository/nishal/newpipeline/samplejava/latest/samplejava-latest.jar /opt/jboss/wildfly/standalone/deployments/
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
EXPOSE 8080
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
